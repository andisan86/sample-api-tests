# API tests using Postman

## Pre-requisites
1. Install [Postman](https://www.postman.com/)
2. Install [Newman](https://www.npmjs.com/package/newman):
```
npm install -g newman
```
3. Install Newman's HTML report packages:
```
npm install -g newman-reporter-html
npm install -g newman-reporter-htmlextra
```
4. Target application should be up and running: https://desolate-anchorage-15727.herokuapp.com

## How to run
### Using Postman
1. Export the `sample_test.postman_collection.json` into Postman.
2. Either run each request or use Runner to run the whole collection.

### Using Newman
1. In the same directory as `sample_test.postman_collection.json`, execute this command:
```
newman run <collection name>.json -n 1 -r htmlextra
```
2. The HTML report should be generated under `newman` directory.
